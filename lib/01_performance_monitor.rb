def measure(n = 1)
  lap = []
  n.times do
    start = Time.now
    yield
    lap << Time.now - start
  end
  lap.reduce(:+) / lap.length
end

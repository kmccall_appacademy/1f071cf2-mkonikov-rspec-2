def reverser
  result = yield.split(" ")
  result = result.map {|x| x.reverse! }
  result.join(" ")
end

def adder(n=1)
  yield + n
end

def repeater(number=1)
  number.times { yield }
end
